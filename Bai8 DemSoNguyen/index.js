var numArr = [];
function ThemSoDuong() {
  var number = document.getElementById("txt-ThemSoDuong").value * 1;

  numArr.push(number);
  //   clear number
  document.getElementById("txt-ThemSoDuong").value = "";

  var contentHMLT = `<h3>Số: ${numArr}</h3>
  `;
  document.getElementById("resultThemSo").innerHTML = contentHMLT;
}

function SoNguyenDauTien() {
  var soNguyenDauTien = null;
  for (let index = 0; index < numArr.length; index++) {
    var num = numArr[index];
    if (num >=1) {
      soNguyenDauTien = num;
      break;
    }
  }

  var contentHTML = `<h3>Số chẵn nhỏ nhất: ${soNguyenDauTien}</h3>`;
  document.getElementById("resultTinhTong").innerHTML = contentHTML;
}
